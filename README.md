** Introduction **

This tutorial is designed to introduce you to a broad range of computer science topics, so you learn a little about each one. This represents the horizontal bar in [T shaped skills](https://en.wikipedia.org/wiki/T-shaped_skills). Knowing a little about a lot of different topics is a good thing because the internet is powerful and you can learn a lot through google, but only if you know what to search for. Programmers and IT people in general rely on google a lot, but they are only effective in doing so because they have a strong base in concepts and general theory. This tutorial attempts to provide the general theory necessary to be an effective programmer. 

** Why did I write my own tutorial? **

There are a lot of resources online for learning programming. There are tons of [websites](http://www.hongkiat.com/blog/sites-to-learn-coding-online/) that try to teach programming online. These websites may have video lectures or questionably helpful coding prompts designed to give you a safe and forgiving environment to write code in. They also attempt to teach you a given language. Maybe that works for some people, but it doesn't feel right to me. Videos take too long to watch, and helpful programming environments feel too fake. I wanted a tutorial where you immediately start writing code exactly as a programmer would. My tutorial is not designed to teach you how to use a single language. Instead it is designed to teach you all the background computer science knowledge that you would get in a 4 year bachelor's degree in Computer Science. Hopefully I can make that as easy as possible, but it may help a lot to have someone you can go to for help if you forget a semicolon. Heh, it's common when you're first starting to write code to have some trouble matching braces, parenthesis, and semicolons. The computer can be unforgiving and may not always give the best error messages. A good defense against this is proper indentation. There are [online tools](http://jsbeautifier.org/) that will do this for you, but it's good to get in the habit of writing well formatted code so you can more easily see errors and understand meaning.


** Getting Started **

If you're a complete beginner to computers and not just to programming, [this website](http://tutorial.djangogirls.org/) looks really good.

The first problem is how to download the examples and problems to your computer so that you can actually start coding.

1. Make sure you understand what source control is:  
   [Neat Read](http://www.codenewbie.org/blogs/what-is-source-control)  
   [Neat Video](https://www.youtube.com/watch?v=K0mgc3efx-A)  
2. This tutorial uses [git](http://msysgit.github.io/). Install that.
3. Decide where you want to store your coding projects. On windows, I like to keep my code in C:/dev. On linux or mac, I use ~/dev.
4. Open a command prompt in whatever directory you decided on, and type

```
#!command line

git clone https://mblandfo@bitbucket.org/mblandfo/theory.git
```
This should create a directory called theory. Go to this directory and you will see all the code. 

Ok, now that you have the code, you need a way to easily view and edit it. If you don't already have a preferred [IDE](https://www.google.com/search?q=IDE), I recommend either [Visual Studio Code](https://code.visualstudio.com/Download) or [Atom](https://atom.io/). These are both free, [cross platform](https://www.google.com/search?q=cross%20platform), and will work well for the tasks in this tutorial.

To commit changes to the git repo, let's use [tortoise git](https://tortoisegit.org/download/). Install that. Then, after making changes, you can right click your directory and select Git Commit. You will need to enter a message describing your commit. You can also review the list of files you are about to commit. Once you hit ok, the changes will be committed to your local repository. A second dialog then pops up with a push button. Hit that, and your changes will be pushed to the bitbucket repository. It should ask you for your password. This is a simple git workflow, in practice you may do other things such as branches and pull requests, but we won't worry about those now.



** END, For now **

** Only notes for Mike beyond this point **

** What will you learn in this tutorial? **

** Learn theory by doing excercises **

+ String manipulation, Regex -> Implement autocomplete. http://www.freescrabbledictionary.com/twl06/download/twl06.txt
+ Primitive Types. Bits, bytes, int, double, floating point, signed/unsigned
+ String encodings
+ Automation
+ Recursion - sierpinski triangle
+ Data Structures. Array, List, Linked List, Dictionary, Hashset, Binary Tree
+ Algorithms. Binary Search. Tree Traversal, O(n), P vs NP, hashing
+ Interfaces and Abstract Classes, Polymorphism
+ Concurrency. Deadlocks, locks/mutex, shared resources
+ Unit Testing, TDD
+ Design Patterns. Singleton. Composition over Inheritance
+ Async

** Required readings **

+ Internationalization. Resource strings, locales, timezones
+ Security. SQL Injection, etc
+ Code Organization, Refactoring, DRY, magic numbers, coding standards and consistency
+ Compilers, Interpreters
+ Continuous Integration, Agile Development (barf), project management, project estimation
+ UX, Visual Design
+ YAGNI, scope creep, etc
+ Programming cartoons, blogs
+ Mythical Man Month
+ Software Licensing. GPL, MIT, etc
+ Cryptography. Don’t write your own
+ Cloud Computing
+ Programming Languages
+ Databases. Indexes, Deadlocks, etc

** Recommended Readings **

+ http://jacquesmattheij.com/the-no-true-programmer-fallacy
+ https://news.ycombinator.com/

Learn web development through small projects

+ Javascript Syntax
+ Knockoutjs
+ JQuery
+ Typescript
+ Grunt
+ NPM
+ CSS, LESS
+ Promises
+ Flexbox