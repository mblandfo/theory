// Utility functions around problem setup and tests

function Util() {}


/** results ca */ 
Util.prototype.outputResults = function(name, actual, expected){    
    var tbl = $('<table>');
    tbl.append(`<th><td>input</td><td>expected</td><td>actual</td></th>`);
    _.each(expected, function(v, i){
        var a = actual[i];
        var correct = a === v;
        var color = correct ? 'green' : 'red';
        var style = `style="color:${color}"`; 
        tbl.append(`<tr><td>${i}</td><td>${v}</td><td ${style}>${a}</td></tr>`); 
    });
    $('body').append('<div class="message"></div>');
    $('body').append(tbl);
}

Util.prototype.computeResults = function(func, min, max, timeoutMs){
    timeoutMs = timeoutMs || 100;
    min = min || 0;
    max = max || 30;
    
    var results = [];
    for(var i=min;i<=max;i++){
        var start = new Date().getTime();
        results.push(func(i));
        var end = new Date().getTime();
        var timeMs = end - start;
        if(timeMs > timeoutMs){
            results.push("TOO SLOW " + timeMs + "ms.");
            break;
        }
    }
    return results;
}