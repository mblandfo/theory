

/** The fibonacci function recursively calculates the nth fibonacci number. 
 * fibonacci(0) = 1
 * fibonacci(1) = 1
 * fibonacci(n) = fibonacci(n-1) + fibonacci(n-2) */
function fibonacci(n) {

}



/** Passing a function to the jquery constructor like below will
 * run the function as soon as the page is loaded. */
$(function(){
    var sb = [];
    for(var i=0;i<50;i++){
        var start = new Date().getTime();
        sb.push(fibonacci(i));
        var end = new Date().getTime();
        var timeMs = end - start;
        if(timeMs > 100){
            sb.push("TOO SLOW " + timeMs + "ms.");
            sb.push(" Why is recursion slow in this case? You could write an iterative solution instead, or you could use memoing.")
            break;
        }
    }
    var result = sb.join(',');
    $('.fibonacci .actual').text(result);
    var correct = result === $('.fibonacci .expected').text();
    var color = correct ? 'green' : 'red';
    $('.fibonacci .actual').css('color', color);
});






